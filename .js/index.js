let x;
    do {
        x = prompt("Введіть будь-яке значення x:");
    } while (!/^\d+$/.test(x));

let y;
    do {
        y = prompt("Введіть будь-яке значення y:");
    } while (!/^\d+$/.test(y));

let calculation;
    do {
        calculation = prompt("Оберіть вид математичної операції: (+, -, *, /)");
    } while (!/^[+\-*/]$/.test(calculation));

function calculationResult (x, y, calculation) {
    switch(calculation) {
        case "+":
            return x + y;
        case "-":
            return x - y;
        case "*":
            return x * y;
        case "/":
            return x / y;
    }
}

let functionResult = calculationResult(Number(x), Number(y), calculation);
    console.log(functionResult);